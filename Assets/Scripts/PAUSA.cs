﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PAUSA : MonoBehaviour
{

    private bool juegoPausado = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (juegoPausado)
            {
                Reanudar();
            }
            else
            {
                Pausa();
            }
        }
    }

    [SerializeField] 
    private GameObject LaPausa;


    [SerializeField] 
    private GameObject menuPausa;

    public void Pausa()
    {
        SoundManager.MusicPausa(true);
        juegoPausado = true;
        Time.timeScale = 0f;
        LaPausa.SetActive(false);
        menuPausa.SetActive(true);
    }

    public void Reanudar()
    {
        SoundManager.MusicPausa(false);
        juegoPausado = false;
        Time.timeScale = 1f;
        LaPausa.SetActive(true);
        menuPausa.SetActive(false);
    }

    /*
    public void Reiniciar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    public void Cerrar()
    {
        Application.Quit();
    }
    */
    
}
