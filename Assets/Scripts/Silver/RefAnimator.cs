﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefAnimator : MonoBehaviour
{
    Animator AnimacionComp;

    // Start is called before the first frame update
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Corriendo(bool State) 
    {
        
        AnimacionComp.SetBool("SilverCorriendo", State);
    }

    public void Saltando(bool State)
    {
        AnimacionComp.SetBool("SilverCorriendo", false);
        AnimacionComp.SetBool("SaltoInicio", State);
        AnimacionComp.SetBool("SaltoFinal", !State);
    }

    public Animator GetAnimator ()
    {
        return AnimacionComp;
    }
}
