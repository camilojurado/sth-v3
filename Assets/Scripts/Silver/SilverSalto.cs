﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SilverSalto : MonoBehaviour
{

    [SerializeField]

    float Salto = 3.0f;



    RefAnimator silver;

    public float fallMultiplay = 2.5f;

    public float lowJumpmultiplayer = 2f;

    
    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    

    // Start is called before the first frame update
    void Start()
    {
        silver = GetComponent<RefAnimator>();
    }

    // Update is called once per frame
    void Update()
    {
        Saltando();
        
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplay - 1) * Time.deltaTime;
            silver.GetAnimator ().SetBool("SaltoInicio", false);
            silver.GetAnimator().SetBool("SaltoFinal", true);
        }
        else if (rb.velocity.y > 0 && !Input.GetButton ("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpmultiplayer - 1) * Time.deltaTime;
            silver.GetAnimator().SetBool("SaltoInicio", true);
            silver.GetAnimator().SetBool("SaltoFinal", false);

        }
        else
        {
            silver.GetAnimator().SetBool("SaltoInicio", false);
            silver.GetAnimator().SetBool("SaltoFinal", false);
        }
        
    }

    void Saltando()
    {
        
        if (Input.GetKeyDown("space"))
        {
            //silver.Saltando(true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * Salto, ForceMode2D.Impulse);
        }
        
        
    }
	
	



}
