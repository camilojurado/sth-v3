﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntos : MonoBehaviour
{
    public int ValPunTos = 0;
    public int ValAnillos = 0;
    public int ValEnemigo = 0;

    
    public Text PunTos;
    public Text Anillos;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        PunTos.text =  ValPunTos.ToString("0000000");
        Anillos.text = ValAnillos.ToString();
    }
}
