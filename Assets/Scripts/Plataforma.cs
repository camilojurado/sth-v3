﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public Transform[] target;
    public float speed = 6.0f;

    int curPos = 0;
    int nextPos = 1;


    private void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, target[nextPos].position, speed * Time.deltaTime);

        if(Vector2.Distance(transform.position, target[nextPos].position) <= 1)
        {
            curPos = nextPos;
            nextPos++;


            if (nextPos > target.Length - 1)
                nextPos = 0;
           

            

        }

    }

    void OnCollisionEnter2D (Collision2D Ob)
    {
        if (Ob.gameObject.CompareTag("Player"))
        {
            Ob.gameObject.transform.parent = gameObject.transform;
        }
    }


    void OnCollisionExit2D(Collision2D Ob)
    {
        if (Ob.gameObject.CompareTag("Player"))
        {
            Ob.gameObject.transform.parent = null;
        }
    }
}
