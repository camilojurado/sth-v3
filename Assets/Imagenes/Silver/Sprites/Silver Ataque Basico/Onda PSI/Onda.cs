﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Onda : MonoBehaviour
{

    Vector2 Direccion;

    [SerializeField]

    int Velocidad = 10;


    [SerializeField]

    int Tiempo = 5;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("TiempoDeOnda");
        Direccion = transform.parent.GetComponent<SpriteRenderer>().flipX ? Vector2.left : Vector2.right;
        GetComponent<SpriteRenderer>().flipX = transform.parent.GetComponent<SpriteRenderer>().flipX;
        transform.parent = null;
        Destroy(gameObject,5);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Direccion * Velocidad * Time.deltaTime);
    }

    IEnumerator TiempoDeOnda()
    {
        yield return new WaitForSeconds(Tiempo);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
