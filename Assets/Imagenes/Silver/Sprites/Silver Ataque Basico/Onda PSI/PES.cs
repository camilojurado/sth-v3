﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PES : MonoBehaviour
{
    [SerializeField]

    Transform origenOnda;

    [SerializeField]

    GameObject Onda;


    public AudioClip SoOnda;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("g"))
        {
            SoundManager.PlaySfx(SoOnda);
            GameObject Wave = Instantiate(Onda, origenOnda.transform.position, origenOnda.transform.rotation, transform);
        }
    }
}
